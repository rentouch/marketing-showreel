#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#===============================================================================
# Written by D.Burnand from Rentouch GmbH 2013 - http://www.rentouch.ch
#===============================================================================

#kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.lang import Builder
from kivy.uix.scatter import Scatter
from kivy.properties import StringProperty
from kivy.core.window import Window
from kivy.clock import Clock
#global
from glob import glob
from random import randint
from os.path import join, dirname

#load kv file
Builder.load_file('main.kv')

class Menu(Widget):
    main_screen_active=True
    def __init__(self, **kwargs):
        super(Menu, self).__init__(**kwargs)
        
        #preload picures for pictures (picturuniverse clone)
        curdir = dirname(__file__)
        for filename in glob(join(curdir, 'images', 'pictureuniverse','*')):
            try:
                picture = Picture(source=filename, rotation=randint(-30,30))
                self.pictures_placeholder.add_widget(picture)
            except:
                print "failed to load image:"+str(filename)
    
        #remove autoloaded kv widgets
        self.remove_widget(self.wetter_image)
        self.remove_widget(self.karte_image)
        self.remove_widget(self.back_button)
        self.remove_widget(self.pictures_placeholder)
        
        #Bad hack for start video after ending of the video
        self.video.bind(eos=self.start_video)
        
    def show_wetter(self, *kwargs):
        '''Display wetter image
        '''
        if self.main_screen_active:
            if self.wetter_image not in self.children:
                self.add_widget(self.wetter_image)
            if self.back_button not in self.children:
                self.add_widget(self.back_button)
            self.main_screen_active=False
    
    def show_karte(self, *kwargs):
        '''Display karte image
        '''
        if self.main_screen_active:
            if self.karte_image not in self.children:
                self.add_widget(self.karte_image)
            if self.back_button not in self.children:
                self.add_widget(self.back_button)
            self.main_screen_active=False
    
    def show_pictures(self, *kwargs):
        '''Load / display scatter images with boarder
        '''
        if self.main_screen_active:
            if self.pictures_placeholder not in self.children:
                self.add_widget(self.pictures_placeholder)
                #center all Pictures (all scatter images in this case)
                for wid in self.pictures_placeholder.children:
                    if wid.__class__.__name__=="Picture":
                        wid.center=Window.center
            if self.back_button not in self.children:
                self.add_widget(self.back_button)
            self.main_screen_active=False
    
    def show_main_screen(self, *kwargs):
        '''Unload all, go back to the main screen
        '''
        if self.wetter_image in self.children:
            self.remove_widget(self.wetter_image)
        if self.karte_image in self.children:
            self.remove_widget(self.karte_image)
        if self.pictures_placeholder in self.children:
            self.remove_widget(self.pictures_placeholder)
        if self.back_button in self.children:
            self.remove_widget(self.back_button)
        self.main_screen_active=True
        
    def start_video(self, *kwargs):
        '''Start video (again)
        '''
        self.video.play=False
        Clock.schedule_once(self.start_video_2)
        print "shedule"
    
    def start_video_2(self, *kwagrs):
        self.video.play=True
        
class Picture(Scatter):
    #dummy class for .kv file
    source = StringProperty(None)
    #add max and min scale size of the scatter image
    scale_min=0.8
    scale_max=2.5
            
class MarketingShowReel(App):
    def build(self):
        return Menu()
MarketingShowReel().run()